package org.example;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ApplicationServiceJUnitTest {
    ApplicationService applicationService;
    CalculatorService calculatorService;


    @BeforeEach
    void init(){
        calculatorService = new CalculatorServiceImpl();
        applicationService = new ApplicationService(calculatorService);
    }

    @ParameterizedTest
    @CsvSource(value = {"10,4,14", "6,8,14", "8,9,17"}, delimiter = ',')
    void addTestForValidNumbers(int a, int b, int expectedResult){
        double actualResult = applicationService.add(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @ParameterizedTest
    @CsvSource(value = {"-10,4", "6,-8", "-8,-9"}, delimiter = ',')
    void addTestForInvalidNumbers(int a, int b){
        Exception exception = Assertions.assertThrows(RuntimeException.class,
                () -> applicationService.add(a, b));

        String expectedMessage = "addition exception: both a and b must be positive numbers";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest
    @CsvSource(value = {"10,6,60", "2,20,40", "4,6,24"}, delimiter = ',')
    void multiplyTestForValidNumbers(int a, int b, int expectedResult){
        double actualResult = applicationService.multiply(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @ParameterizedTest
    @CsvSource(value = {"9,10", "12,8", "9,8"}, delimiter = ',')
    void multiplyTestForInvalidNumbers(int a, int b){
        Exception exception = Assertions.assertThrows(RuntimeException.class,
                () -> applicationService.multiply(a, b));

        String expectedMessage = "multiplication exception: a is grt 4, b is grt 6";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest
    @CsvSource(value = {"15,10,5", "12,20,-8", "30,27,3"}, delimiter = ',')
    void subtractTestForValidNumbers(int a, int b, int expectedResult){
        double actualResult = applicationService.subtract(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @ParameterizedTest
    @CsvSource(value = {"10,8", "12,6", "9,5"}, delimiter = ',')
    void subtractTestForInvalidNumbers(int a, int b){
        Exception exception = Assertions.assertThrows(RuntimeException.class,
                () -> applicationService.subtract(a, b));

        String expectedMessage = "subtraction exception: 2, 4, 6 not allowed for result";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest
    @CsvSource(value = {"16,4,4", "12,4,3", "18,6,3"}, delimiter = ',')
    void divideTestForValidNumbers(int a, int b, int expectedResult){
        double actualResult = applicationService.divide(a, b);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @ParameterizedTest
    @CsvSource(value = {"9,3", "20,5", "39,13"}, delimiter = ',')
    void divideTestForInvalidNumbers(int a, int b){
        Exception exception = Assertions.assertThrows(RuntimeException.class,
                () -> applicationService.divide(a, b));

        String expectedMessage = "division exception: both a and b must be even numbers";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest
    @CsvSource(value = {"6", "8", "10"})
    void divideTestForDivisionByZero(int a){
        Exception exception = Assertions.assertThrows(RuntimeException.class,
                () -> applicationService.divide(a, 0));

        String expectedMessage = "division exception: division by zero";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }
}
