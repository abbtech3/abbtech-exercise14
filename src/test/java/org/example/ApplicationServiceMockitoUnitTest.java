package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ApplicationServiceMockitoUnitTest {

    @Mock
    private CalculatorService calculatorService;
    @InjectMocks
    private ApplicationService applicationService;

    @Test
    void testAdd_WhenCalculatorServiceIsCalled(){
        int a = 5;
        int b = 4;
        int expected = 9;
        Mockito.when(calculatorService.add(a, b)).thenReturn(expected);

        int actual = applicationService.add(a, b);

        Assertions.assertEquals(expected, actual);
        Mockito.verify(calculatorService, Mockito.times(1)).add(a, b);
    }

    @Test
    void testAdd_WhenCalculatorServiceIsNotCalled(){
        int a = -5;
        int b = -4;
        Assertions.assertThrows(ArithmeticException.class, () -> applicationService.add(a, b));
        Mockito.verify(calculatorService, Mockito.never()).add(a, b);
    }



    @Test
    void testMultiply_WhenCalculatorServiceIsCalled(){
        int a = 5;
        int b = 1;
        int expected = 5;
        Mockito.when(calculatorService.multiply(a, b)).thenReturn(expected);

        int actual = applicationService.multiply(a, b);

        Assertions.assertEquals(expected, actual);
        Mockito.verify(calculatorService, Mockito.times(1)).multiply(a, b);
    }

    @Test
    void testMultiply_WhenCalculatorServiceIsNotCalled(){
        int a = 10;
        int b = 8;
        Assertions.assertThrows(ArithmeticException.class, () -> applicationService.multiply(a, b));
        Mockito.verify(calculatorService, Mockito.never()).multiply(a, b);
    }

    @Test
    void testSubtract_WhenCalculatorServiceIsCalled(){
        int a = 5;
        int b = 4;
        int expected = 1;
        Mockito.when(calculatorService.subtract(a, b)).thenReturn(expected);

        int actual = applicationService.subtract(a, b);

        Assertions.assertEquals(expected, actual);
        Mockito.verify(calculatorService, Mockito.times(1)).subtract(a, b);
    }

    @Test
    void testSubtract_WhenCalculatorServiceIsCalledButExceptionThrown(){
        int a = 6;
        int b = 4;
        int expected = 2;
        Mockito.when(calculatorService.subtract(a, b)).thenReturn(expected);
        Assertions.assertThrows(ArithmeticException.class, () -> applicationService.subtract(a, b));
        Mockito.verify(calculatorService, Mockito.times(1)).subtract(a, b);
    }

    @Test
    void testDivide_WhenCalculatorServiceIsCalled(){
        int a = 12;
        int b = 4;
        int expected = 3;
        Mockito.when(calculatorService.divide(a, b)).thenReturn(expected);

        int actual = applicationService.divide(a, b);

        Assertions.assertEquals(expected, actual);
        Mockito.verify(calculatorService, Mockito.times(1)).divide(a, b);
    }

    @Test
    void testDivideForInvalidCase_WhenCalculatorServiceIsNotCalled(){
        int a = 7;
        int b = 1;
        Assertions.assertThrows(ArithmeticException.class, () -> applicationService.divide(a, b));
        Mockito.verify(calculatorService, Mockito.never()).divide(a, b);
    }
}
