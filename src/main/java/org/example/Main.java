package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Main {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/abb_tech";
    private static final String USER_NAME = "psg_user";
    private static final String PASS = "pass";
    public static void main(String[] args) {
//        ApplicationService applicationService = new ApplicationService(new CalculatorServiceImpl());
//        System.out.println(applicationService.add(4, 5));
//        System.out.println(applicationService.multiply(4, 8));
//        System.out.println(applicationService.subtract(8, 5));
//        System.out.println(applicationService.divide(8, 4));

        Connection connection = null;
        String sqlInsert = """
                insert into calculation."CALCULATION_RESULT"(variable_a, variable_b, calc_result, calc_method)\s
                values(?, ?, ?, ?);
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setInt(1, 2);
            preparedStatement.setInt(2, 3);
            preparedStatement.setInt(3, 6);
            preparedStatement.setString(4, "multiply");
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }
}