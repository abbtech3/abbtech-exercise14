package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CalculationRepository {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/abb_tech";
    private static final String USER_NAME = "psg_user";
    private static final String PASS = "pass";

    public void saveCalculationResult(int a, int b, int result, String calculationMethod){
        Connection connection = null;
        String sqlInsert = """
                insert into "CALCULATION".calculation_result(variable_a,variable_b,calc_result,calc_method)
                 values (?,?,?,?)
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setInt(1, a);
            preparedStatement.setInt(2, b);
            preparedStatement.setInt(3, result);
            preparedStatement.setString(4, calculationMethod);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }


    }
}
