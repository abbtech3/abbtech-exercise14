package org.example;

public class ApplicationService {
    private final CalculatorService calculatorService;

    public ApplicationService(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    public int multiply(int a, int b) {
        if (a > 4 && b > 6) {
            throw new ArithmeticException("multiplication exception: a is grt 4, b is grt 6");
        }
        return calculatorService.multiply(a, b);
    }

    public int subtract(int a, int b) {
        var result = calculatorService.subtract(a, b);
        if (result == 2 || result == 4 || result == 6 || result == 8) {
            throw new ArithmeticException("subtraction exception: 2, 4, 6 not allowed for result");
        }
        return result;
    }

    public int add(int a, int b){
        if(a <= 0 || b <=0){
            throw new ArithmeticException("addition exception: both a and b must be positive numbers");
        }
        return calculatorService.add(a, b);
    }

    public int divide(int a, int b){
        if(a % 2 != 0 || b % 2 != 0){
            throw new ArithmeticException("division exception: both a and b must be even numbers");
        }
        return calculatorService.divide(a, b);
    }

}
