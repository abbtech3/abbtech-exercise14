package org.example;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CalculationServlet",
        urlPatterns = {"/calculate"})
public class CalculationServlet extends HttpServlet {

    private ApplicationService applicationService;

    private CalculationRepository calculationRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        applicationService = new ApplicationService(new CalculatorServiceImpl());
        calculationRepository = new CalculationRepository();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();

        try {
            String calculationMethod = req.getHeader("x-calculation-method");
            if(calculationMethod == null){
                throw new NullPointerException("header not sent ...");
            }
            calculationMethod = calculationMethod.toLowerCase();
            String parA = req.getParameter("a");
            String parB = req.getParameter("b");
            if(parA == null || parB == null){
                throw new NumberFormatException("query parameter is missing ...");
            }
            int a = Integer.parseInt(parA);
            int b = Integer.parseInt(parB);
            int result = 0;

            switch (calculationMethod) {
                case "multiply":
                    result = applicationService.multiply(a, b);
                    break;
                case "add":
                    result = applicationService.add(a, b);
                    break;
                case "divide":
                    result = applicationService.divide(a, b);
                    break;
                case "subtract":
                    result = applicationService.subtract(a, b);
                    break;
                default:
                    throw new ArithmeticException("not a valid operator");
            };
            calculationRepository.saveCalculationResult(a, b, result, calculationMethod);
            writer.write("{\n" +
                    "    \"result\": \"" + result + "\"\n" +
                    "}");
        } catch (RuntimeException exception) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("{\n" +
                    "    \"exception\": \"" + exception.getMessage() + "\"\n" +
                    "}");
        }
        resp.setContentType("application/json");
    }
}
